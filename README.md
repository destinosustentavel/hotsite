# Hotsite

Hotsite do Destino Sustentável.

## Ferramentas Utilizadas

* [React](https://pt-br.reactjs.org/)

## Configuração do Ambiente

Para rodar este projeto no seu computador, é necessário que você tenha instalado o [Node.js](https://nodejs.org/en/download/) e um editor de código de sua preferência para facilitar a visualização dos arquivos. Para isso recomendamos o [Visual Studio Code](https://code.visualstudio.com/download) por ser de código aberto e fácil de utilizar.

## Como Utilizar

```bash
# clone este repositório
git clone https://gitlab.com/destinosustentavel/editor.git

# instale as dependências
npm install

# execute a aplicação
npm start
```

## Como Contribuir

* Faça fork deste repositório
* Realize as mudanças no código
* Submeta um merge request
* Aguarde a avaliação da equipe de desenvolvimento

## Sobre a aplicação

Para fazer qualquer alteração de estrutura da página ou do seu conteúdo, modifique o arquivo `App.js`. Já para alterar a estilização, modifique o arquivo com a extensão `.scss` de acordo com seção da aplicação e execute `npm run scss-compile` no seu terminal para gerar o arquivo `style.css` com as suas modificações.

## Licença

[MIT License](https://opensource.org/licenses/MIT)
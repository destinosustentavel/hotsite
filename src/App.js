import { FaFacebook, FaInstagram, FaTwitter, FaGooglePlay } from "react-icons/fa";
import { SiAndroid } from "react-icons/si";

import logo from "./images/logo.png";
import appQrCode from "./images/appqrcode.png";

import "./styles/css/style.css";

export default function App() {
  return (
    <div className="container">
      <button className="float-button">
        <a
          href="https://play.google.com/store/apps/details?id=org.destinosustentavel.dsmovel&hl=pt_BR"
          target="blank"
        >
          Ir para o aplicativo
        </a>
      </button>

      <section className="praticidade background-green">
        <h1 class="page-title">Destino Sustentável agora é aplicativo também</h1>

        <div className="praticidade-conteudo">
          <div>
            <h1 className="section-title">+ Praticidade</h1>
            <p>É com grande felicidade que apresentamos o aplicativo do Destino Sustentável para smartphones Android.</p>
            <div className="logos-container">
              <SiAndroid size={110} color="#fff" />
              <FaGooglePlay size={100} color="#fff" />
              <img src={appQrCode} alt="Aplicativo"/>
            </div>
          </div>

          <div className="logo-container">
            <img src={logo} alt="Destino Sustentável"/>

            <button>
              <a href="https://destinosustentavel.com.br/" target="blank">Ir para o site</a>
            </button>
          </div>
        </div>
      </section>

      <section className="funcionalidades background-white">
        <h1 className="section-title">+ Funcionalidades</h1>

        <div className="funcionalidades-conteudo">
          
          {/* oferta */}
          {/* pedidos */}
          {/* bate papo */}
          {/* doação/cormecialização */}

          <div className="funcionalidade-item">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png" alt="Funcionalidade 1"/>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>

          <div className="funcionalidade-item">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png" alt="Funcionalidade 2"/>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>

          <div className="funcionalidade-item">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png" alt="Funcionalidade 3"/>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>

          <div className="funcionalidade-item">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png" alt="Funcionalidade 4"/>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
        </div>
      </section>

      <section className="interatividade background-green">
        <h1 className="section-title">+ Interatividade</h1>

        <div className="interatividade-conteudo">
          <p>Veja um breve vídeo de apresentação do aplicativo e venha fazer parte dessa rede a favor do meio ambiente e, consequentemente, das pessoas.<br/>Esperamos você lá!</p>

          <iframe width="1024" height="315" src="https://www.youtube.com/embed/5shVI0AgSDY" title="Destino Sustentável" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </section>

      <section className="final background-white">
        <h1 className="section-title">+ Destino Sustentável</h1>
        <h3 className="section-subtitle">Reciclar para recriar o futuro</h3>

        <div className="final-conteudo">
          <p>Acompanhe mais novidades como esta, notícias e dicas de sustentabilidade em nossos canais de comunicação.<br/>Ficaremos felizes em ter você como nosso seguidor!</p>

          <div className="social-icons">
            <a href="https://www.facebook.com/aplicativodestsustentavel" target="blank">
              <FaFacebook size={40} color="#007B36" />
            </a>

            <a href="https://www.instagram.com/destsustentavel/" target="blank">
              <FaInstagram size={40} color="#007B36" />
            </a>

            <a href="https://twitter.com/sustentaveldest" target="blank">
              <FaTwitter size={40} color="#007B36" />
            </a>
          </div>
        </div>
      </section>
    </div>
  );
}